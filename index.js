const { ApolloServer, gql } = require('apollo-server');

const genres = [
  {
    genre: 'Action/Adventure',
  },
  {
    genre: 'Drama/Crime',
  },
  {
    genre: 'Thriller, Action, Crime',
  },
  {
    genre: 'Action, Thriller, Crime',
  },
];

const typeDefs = gql`
  type Genre {
    genre: String!
    movies: [Movie!]
  }

  type Movie {
    title: Title
    genre: String
    year: Int
    rating: Int
    url: String
  }

  type Title {
    title: String
  }

  type Query {
    movies: [Movie]
    genres: [Genre]
  }

  type Mutation {
    logger(error: String!): Boolean
  }
`;

const movies = [
  {
    title: 'The Dark Knight',
    genre: 'Action/Adventure',
    year: 2008,
    rating: 85,
    url: 'https://www.themoviedb.org/movie/155-the-dark-knight',
  },
  {
    title: 'The Godfather',
    genre: 'Drama/Crime',
    year: 1972,
    rating: 87,
    url: 'https://www.themoviedb.org/movie/238-the-godfather',
  },
  {
    title: 'John Wick: Chapter 2',
    genre: 'Thriller, Action, Crime',
    year: 2017,
    rating: 72,
    url: 'https://www.themoviedb.org/movie/324552-john-wick-chapter-2',
  },
  {
    title: 'John Wick: Chapter 3 - Parabellum',
    genre: 'Action, Thriller, Crime',
    year: 2019,
    rating: 74,
    url: 'https://www.themoviedb.org/movie/458156-john-wick-chapter-3',
  },
];

const resolvers = {
  Query: {
    genres: () => genres,
  },
  Genre: {
    movies(parent) {
      return movies.filter((movie) => movie.genre === parent.genre);
    },
  },
  Movie: {
    title(parent) {
      return {
        title: parent.title,
      };
    },
  },
  Mutation: {
    logger: (parents, args, context, info) => {
      console.log('Doslo je do greske');
      console.log(args.error);
      return true;
    },
  },
};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
